import csv
import os
import argparse
import datetime

#data dictonary
dict_file = {
    1: {1 : "adresse_titulaire"}, 
    2: {10: "carrosserie"},
    3: {11: "categorie"},
    4: {9: "couleur"},
    5: {12: "cylindre"},
    6: {5: "date_immatriculation"},
    7: {8: "denomination_commerciale"},
    8: {13: "energie"},
    9: {3: "prenom"},
    10: {4: "immatriculation"},
    11: {7: "marque"},
    12: {2: "nom"},
    13: {14: "places"},
    14: {15: "poids"},
    15: {16: "puissances"},
    16: {17: "type", 18: "variante", 19: "version"},
    17: {6: "vin"}
}

#data table
tab_file = [1,10,11,9,12,5,8,13,3,4,7,2,14,15,16,17,6]

def check_file_path(file_path):
    """
    IN: the file path to transform
    OUT: the value of the tests 'true' or 'false'
    this function test if the file exist and if it's a file.
    """
    if not(os.path.exists(file_path)):
        print("This file doesn't exist !")
        return False
    if not(os.path.isfile(file_path)):
        print("This file is not a file !")
        return False
    return True

def check_delimiter(delimiter):
    '''
    IN: the delimiter chosen by the consumer
    OUT: the delimiter chosen by the consumer or ';' if the chosen delimiter is not good (security)
    this function test if the delimiter chosen by the consumer is good if it's not the case it returns ';'
    '''
    if delimiter is not None:
        if isinstance(delimiter, str) and len(delimiter) == 1:
            return delimiter
        raise argparse.ArgumentTypeError("The delimiter is not a caracter !")
    return ';' #security

def reading(file_path, delimiter):
    """
    IN: the file path and de delimiter of the data
    This function reads line by line the csv file and for each lines, call the writing function.
    """
    with open(file_path) as csvfile:
        reader = csv.reader(csvfile, delimiter="|")
        header = True
        for fieldline in reader:
            if header:         
                header = False   
                writing(writing_header(), delimiter)
            else:
                writing(writing_lines(fieldline), delimiter)

def writing_header():
    """
    OUT: 'line' : the new line created
    this functrion return a line of the header. 
    """
    line = ['']*19
    for pro_index in range(17):
        fm_index = tab_file[pro_index]
        if pro_index == 15:
            line[fm_index-1] = dict_file[pro_index+1][fm_index]
            line[fm_index] = dict_file[pro_index+1][fm_index+1]
            line[fm_index+1] = dict_file[pro_index+1][fm_index+2]
        else:
            line[fm_index-1] = dict_file[pro_index+1][fm_index]
    return line

def writing_lines(fieldline):
    """
    IN: the line to transcribe 
    OUT: 'line' : the new line created
    this function return the new line transcribed. Change the date format if it's necessary 
    """
    line = ['']*19
    pro_index = 0
    for datafield in fieldline:
        fm_index = tab_file[pro_index]
        if pro_index == 15:
            section = datafield.split(',')
            line[fm_index-1] = section[0]
            line[fm_index] = section[1].replace(' ','')
            line[fm_index+1] = section[2].replace(' ','')
        else:
            if pro_index == 5:
                current_date = datetime.datetime.strptime(datafield, '%Y-%m-%d')
                line[fm_index-1] = current_date.strftime('%d/%m/%Y')
            else:
                line[fm_index-1] = datafield
        pro_index = pro_index + 1
        
    return line

def writing(line, delimiter):
    """
    IN: the line to write and the delimiter chosen by the consumer
    this function will write a line used as parameters in 'result.csv'.
    """
    with open('result.csv', 'a', newline='') as csvfile:         
        writer = csv.writer(csvfile, delimiter=delimiter)
        writer.writerow(line)

if __name__=="__main__":
    print("|----------------------------------------------------|")
    print("\tReady to change the data ?")
    print("|----------------------------------------------------|")

    parser = argparse.ArgumentParser(description="READ JOJO !!")
    parser.add_argument("filepath", help="the file path")
    parser.add_argument("delimiter", help="the delimiter", type = check_delimiter)
    args = parser.parse_args()

    with open('result.csv', 'w') as csvfile:
        csvfile.write("")
    if check_file_path(args.filepath):
        reading(args.filepath, args.delimiter)
    else:
        print("THE END !")

    print("\tDone yet")
    print("\n|----------------------------------------------------|")
