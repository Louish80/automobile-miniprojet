import unittest
import csv
from file import *

class ImportTestCase(unittest.TestCase):

    def setUp(self):
        csvfile = open("test.csv", "w")
        csvfile.write("")
        csvfile.close()
        tableTest = ['']*2
        tableTest[0] = ["address","carrosserie","categorie","couleur","cylindree","date_immat","denomination","energy","firstname","immat","marque","name","places","poids","puissance","type_variante_version","vin"]
        tableTest[1] = ["3822 Omar Square Suite 257 Port Emily, OK 43251","45-1743376","34-7904216","LightGoldenRodYellow","3462","2012-05-03","Enhanced well-modulated moderator","37578077","Jerome","OVC-568","Williams Inc","Smith","32","3827","110","Inc, 92-3625175, 79266482","9780082351764"]

        with open('test.csv', 'a', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter='|')
            for ligne in tableTest:
                writer.writerow(ligne)
    
    def test_writing_header(self):
        header = ['']
        header = ["adresse_titulaire","nom","prenom","immatriculation","date_immatriculation","vin","marque","denomination_commerciale","couleur","carrosserie","categorie","cylindre","energie","places","poids","puissances","type","variante","version"]
        self.assertEqual(writing_header(), header)

    def test_writing_lines(self):
        line2 = []
        line2 = ["3822 Omar Square Suite 257 Port Emily, OK 43251","Smith","Jerome","OVC-568","03/05/2012","9780082351764","Williams Inc","Enhanced well-modulated moderator","LightGoldenRodYellow","45-1743376","34-7904216","3462","37578077","32","3827","110","Inc","92-3625175","79266482"]
        line3 = []
        line3 = ["974 Byrd Mountains New Jennifer, IL 01612","Olson","Paul","859 GZP","14/06/2019","9781246585674","Ross PLC","Right-sized secondary array","Snow","03-0258606","71-7143342","4837","15895400","9","5870","298","LLC","37-7112501","39890658"]
        lines = ['']*3
        index = 0

        with open('test.csv') as csvfile:
            reader = csv.reader(csvfile, delimiter="|")
            for row in reader:
                lines[index] = row
                index = index +1
        
        self.assertEqual(writing_lines(lines[1]), line2)
        self.assertEqual(writing_lines(lines[2]), line3)


